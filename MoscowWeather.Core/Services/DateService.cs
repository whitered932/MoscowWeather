namespace MoscowWeather.Core.Services;

public class DateService : IDateService
{
    public DateTime NowUtc()
    {
        return DateTime.UtcNow;
    }
}
namespace MoscowWeather.Core.Services;

public interface IDateService
{
    public DateTime NowUtc();
}
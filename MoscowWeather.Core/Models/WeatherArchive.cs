using MoscowWeather.Core.Enums;

namespace MoscowWeather.Core.Models;

public class WeatherArchive : BaseModel
{
    private WeatherArchive() {}
    
    public WeatherArchive(byte[] content, DateTime uploadedAt, string name)
    {
        Status = ArchiveStatus.Loaded;
        Content = content;
        UploadedAt = uploadedAt;
        Name = name;
    }

    public string Name { get; private set; }
    public DateTime UploadedAt { get; private set; }
    public byte[] Content { get; private set; }
    public ArchiveStatus Status { get; private set; }

    public string? ParsingJobId { get; private set; }
    public void SetParsingJobId(string jobId)
    {
        if (string.IsNullOrEmpty(ParsingJobId))
        {
            ParsingJobId = jobId;
        }
    }
    public void UpdateStatus(ArchiveStatus status)
    {
        Status = status;
    }
}
namespace MoscowWeather.Core.Models;

public class Weather : BaseModel
{
    private Weather()
    {
    }

    public Weather(
        DateTime dateTimeUtc,
        double? dewPoint,
        double? temperature,
        double? relativeHumidity,
        double? windSpeed,
        double? horizontalVisibility,
        double? lowerCloudLimit,
        string? weatherConditions,
        double? pressure,
        double? cloudy,
        string? windDirection,
        long archiveId)
    {
        DateTimeUtc = dateTimeUtc;
        DewPoint = dewPoint;
        Temperature = temperature;
        RelativeHumidity = relativeHumidity;
        WindSpeed = windSpeed;
        HorizontalVisibility = horizontalVisibility;
        LowerCloudLimit = lowerCloudLimit;
        WeatherConditions = weatherConditions;
        Pressure = pressure;
        Cloudy = cloudy;
        WindDirection = windDirection;
        ArchiveId = archiveId;
    }

    public long ArchiveId { get; private set; }
    public DateTime DateTimeUtc { get; private set; }
    public double? DewPoint { get; private set; }
    public double? Pressure { get; private set; }
    public double? Temperature { get; private set; }
    public double? RelativeHumidity { get; private set; }
    public double? WindSpeed { get; private set; }
    public double? HorizontalVisibility { get; private set; }
    public double? LowerCloudLimit { get; private set; }
    public double? Cloudy { get; private set; }
    public string? WeatherConditions { get; private set; }
    public string? WindDirection { get; private set; }
}
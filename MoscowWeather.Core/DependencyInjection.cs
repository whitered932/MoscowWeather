﻿using Microsoft.Extensions.DependencyInjection;
using MoscowWeather.Core.Services;

namespace MoscowWeather.Core;

public static class DependencyInjection
{
    public static IServiceCollection AddCore(this IServiceCollection services)
    {

        services.AddTransient<IDateService, DateService>();
        return services;
    }
    
}
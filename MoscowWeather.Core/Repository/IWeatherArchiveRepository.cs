using MoscowWeather.Core.Models;

namespace MoscowWeather.Core.Repository;

public interface IWeatherArchiveRepository : IRepository
{
    public Task<WeatherArchive?> GetByIdAsync(long id, CancellationToken cancellationToken);
    public Task<List<WeatherArchive>> ListAsync(CancellationToken cancellationToken);
    public Task AddAsync(WeatherArchive archive, CancellationToken cancellationToken);
    public Task RemoveAsync(WeatherArchive archive, CancellationToken cancellationToken);
}
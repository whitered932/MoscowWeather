using MoscowWeather.Core.Models;

namespace MoscowWeather.Core.Repository;

public interface IWeatherRepository : IRepository
{
    public Task<IReadOnlyList<Weather>> ListAsync(int page, int count, long archiveId, List<int> months,
        List<int> years,
        CancellationToken cancellationToken);
    public Task<int> Count(long archiveId, List<int> months, List<int> years, CancellationToken cancellationToken);
    public Task<IReadOnlyList<Weather>> ListAsync(long archiveId, CancellationToken cancellationToken);
    public Task RemoveRangeAsync(IEnumerable<Weather> weathers);
    public Task AddRangeAsync(List<Weather> weathers, CancellationToken cancellationToken);
    public Task<List<int>> GetYears(long archiveId, CancellationToken cancellationToken);

}
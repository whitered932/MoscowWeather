using System.Data.Common;

namespace MoscowWeather.Core.Repository;

public interface IRepository
{
    public Task SaveChangesAsync(CancellationToken cancellationToken);
}
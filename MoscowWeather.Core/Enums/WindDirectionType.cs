namespace MoscowWeather.Core.Enums;

// С точки зрения оптимизации направление ветра можно сохранять как Enum, а не строками
// В данном случае - пока что это не требуется
public enum WindDirectionType
{
    Unknown,
    N,
    NE,
    E,
    SE,
    S,
    SW,
    W,
    NW
}
namespace MoscowWeather.Core.Enums;

public enum ArchiveStatus
{
    Loaded,
    InWork,
    Ready,
    Error,
    Cancelled
}
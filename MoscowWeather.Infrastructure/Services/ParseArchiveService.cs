using Microsoft.Extensions.Logging;
using MoscowWeather.Core.Enums;
using MoscowWeather.Core.Models;
using MoscowWeather.Core.Repository;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace MoscowWeather.Infrastructure.Services;

public class ParseArchiveService : IParseArchiveService
{
    private const int DateCellNum = 0;
    private const int TimeCellNum = 1;
    private const int TemperatureCellNum = 2;
    private const int RelativeHumidityCellNum = 3;
    private const int DewPointCellNum = 4;
    private const int PressureCellNum = 5;
    private const int WindDirectionCellNum = 6;
    private const int WindSpeedCellNum = 7;
    private const int CloudyCellNum = 8;
    private const int LowerCloudLimitCellNum = 9;
    private const int HorizontalVisibilityCellNum = 10;
    private const int WeatherConditionsCellNum = 11;

    private readonly IWeatherArchiveRepository _archiveRepository;
    private readonly IWeatherRepository _weatherRepository;
    private readonly ILogger<ParseArchiveService> _logger;

    public ParseArchiveService(
        IWeatherArchiveRepository archiveRepository,
        IWeatherRepository weatherRepository,
        ILogger<ParseArchiveService> logger)
    {
        _archiveRepository = archiveRepository;
        _weatherRepository = weatherRepository;
        _logger = logger;
    }

    public async Task ParseAsync(long archiveId, CancellationToken cancellationToken)
    {
        var archive = await _archiveRepository.GetByIdAsync(archiveId, cancellationToken);
        if (archive is null)
        {
            return;
        }

        var weatherList = new List<Weather>();
        _logger.LogInformation($"Начало парсинга архива {archive.Id} - {archive.Name}");
        archive.UpdateStatus(ArchiveStatus.InWork);
        await _archiveRepository.SaveChangesAsync(cancellationToken);
        var stream = new MemoryStream(archive.Content);
        var workbook = new XSSFWorkbook(stream);
        for (var i = 0; i < workbook.NumberOfSheets; i++)
        {
            var sheet = workbook.GetSheetAt(i);
            _logger.LogInformation($"Парсинг архива {archive.Id} - {archive.Name}, sheetId - {sheet.SheetName}");
            var rowNum = sheet.LastRowNum;
            for (var j = 4; j <= rowNum; j++)
            {
                var curRow = sheet.GetRow(j);
                if (curRow.Cells.Count == 0)
                {
                    continue;
                }

                var date = curRow.AsString(DateCellNum)?.Trim();
                var time = curRow.AsString(TimeCellNum)?.Trim();
                var temperature = curRow.AsNum(TemperatureCellNum);
                var relativeHumidity = curRow.AsNum(RelativeHumidityCellNum);
                var dewPoint = curRow.AsNum(DewPointCellNum);
                var pressure = curRow.AsNum(PressureCellNum);
                var windDirection = curRow.AsString(WindDirectionCellNum);
                var windSpeed = curRow.AsNum(WindSpeedCellNum);
                var cloudy = curRow.AsNum(CloudyCellNum);
                var lowerCloudLimit = curRow.AsNum(LowerCloudLimitCellNum);
                var horizontalVisibility = curRow.AsNum(HorizontalVisibilityCellNum);
                var weatherConditions = curRow.AsString(WeatherConditionsCellNum)?.Trim();
                DateTime datetime;

                try
                {
                    datetime = DateTime.Parse($"{date} {time}Z").ToUniversalTime();
                }
                catch (FormatException e)
                {
                    // Если какая то дата незапаршена - скорее всего весь архив испорчен.
                    // Смысла строить отчёт на неверных данных нет.
                    _logger.LogError(e.Message);
                    archive.UpdateStatus(ArchiveStatus.Error);
                    await _archiveRepository.SaveChangesAsync(cancellationToken);
                    break;
                }

                var weather = new Weather(
                    datetime,
                    dewPoint,
                    temperature,
                    relativeHumidity,
                    windSpeed,
                    horizontalVisibility,
                    lowerCloudLimit,
                    weatherConditions,
                    pressure,
                    cloudy,
                    windDirection,
                    archive.Id);
                weatherList.Add(weather);
            }
        }

        _logger.LogInformation($"Парсинг архива завершен {archive.Id} - {archive.Name}");
        if (archive.Status is not ArchiveStatus.Error)
        {
            await _weatherRepository.AddRangeAsync(weatherList, cancellationToken);
            archive.UpdateStatus(ArchiveStatus.Ready);
            await _archiveRepository.SaveChangesAsync(cancellationToken);
        }
    }

    // С точки зрения оптимизации направление ветра можно сохранять как Enum, а не строками
    // В данном случае - пока что это не требуется
    private static WindDirectionType GetDirection(string? windDirection)
    {
        var windDirectionType = windDirection switch
        {
            "С" => WindDirectionType.N,
            "СВ" => WindDirectionType.NE,
            "В" => WindDirectionType.E,
            "ЮВ" => WindDirectionType.SE,
            "Ю" => WindDirectionType.S,
            "ЮЗ" => WindDirectionType.SW,
            "З" => WindDirectionType.W,
            "СЗ" => WindDirectionType.NW,
            _ => WindDirectionType.Unknown
        };
        return windDirectionType;
    }
}

public static class RowExtension
{
    public static double? AsNum(this IRow row, int cellNum)
    {
        try
        {
            return row.GetCell(cellNum)?.NumericCellValue;
        }
        catch (InvalidOperationException e)
        {
            return null;
        }
    }

    public static string? AsString(this IRow row, int cellNum)
    {
        try
        {
            return row.GetCell(cellNum)?.StringCellValue;
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
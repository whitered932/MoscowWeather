namespace MoscowWeather.Infrastructure.Services;

public interface IParseArchiveService
{
    public Task ParseAsync(long archiveId, CancellationToken cancellationToken);
}
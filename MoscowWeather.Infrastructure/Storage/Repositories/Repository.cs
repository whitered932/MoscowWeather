using Microsoft.EntityFrameworkCore;
using MoscowWeather.Core.Repository;

namespace MoscowWeather.Infrastructure.Storage.Repositories;

public class Repository<TDbContext> : IRepository where TDbContext : DbContext
{
    protected readonly MoscowWeatherDbContext _dbContext;
    
    protected Repository(MoscowWeatherDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
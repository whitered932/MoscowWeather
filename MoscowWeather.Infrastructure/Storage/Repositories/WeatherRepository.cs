using Microsoft.EntityFrameworkCore;
using MoscowWeather.Core.Models;
using MoscowWeather.Core.Repository;

namespace MoscowWeather.Infrastructure.Storage.Repositories;

public class WeatherRepository : IWeatherRepository
{
    private readonly MoscowWeatherDbContext _dbContext;

    public WeatherRepository(MoscowWeatherDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<Weather?> FindAsync(long id, CancellationToken cancellationToken)
    {
        var weather = await _dbContext.Weathers.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        return weather;
    }

    public async Task<IReadOnlyList<Weather>> ListAsync(
        int page,
        int count,
        long archiveId,
        List<int>? months,
        List<int>? years,
        CancellationToken cancellationToken)
    {
        var skip = (page - 1) * count;
        var query = _dbContext.Weathers
            .Where(x => x.ArchiveId == archiveId);
        if (months?.Count > 0)
        {
            query = query.Where(x => months.Contains(x.DateTimeUtc.Month));
        }

        if (years?.Count > 0)
        {
            query = query.Where(x => years.Contains(x.DateTimeUtc.Year));
        }

        var weathers = await query
            .OrderBy(x => x.Id)
            .Skip(skip)
            .Take(count)
            .ToListAsync(cancellationToken);
        return weathers;
    }

    public async Task<List<int>> GetYears(
        long archiveId,
        CancellationToken cancellationToken)
    {
        return await _dbContext.Weathers
            .Where(x => x.ArchiveId == archiveId).Select(x => x.DateTimeUtc.Year)
            .Distinct()
            .ToListAsync(cancellationToken: cancellationToken);
    }


    public async Task<IReadOnlyList<Weather>> ListAsync(long archiveId, CancellationToken cancellationToken)
    {
        var result = await _dbContext.Weathers
            .Where(x => x.ArchiveId == archiveId)
            .ToListAsync(cancellationToken);
        return result;
    }

    public async Task AddAsync(Weather weather, CancellationToken cancellationToken)
    {
        await _dbContext.Weathers.AddAsync(weather, cancellationToken);
    }

    public async Task RemoveRangeAsync(IEnumerable<Weather> weathers)
    {
        _dbContext.Weathers.RemoveRange(weathers);
    }

    public async Task AddRangeAsync(List<Weather> weathers, CancellationToken cancellationToken)
    {
        await _dbContext.Weathers.AddRangeAsync(weathers, cancellationToken);
    }


    public async Task<int> Count(long archiveId, List<int> months, List<int> years, CancellationToken cancellationToken)
    {
        var query = _dbContext.Weathers.Where(x => x.ArchiveId == archiveId);
        if (months.Count > 0)
        {
            query = query.Where(x => months.Contains(x.DateTimeUtc.Month));
        }

        if (years.Count > 0)
        {
            query = query.Where(x => months.Contains(x.DateTimeUtc.Year));
        }

        return await query.CountAsync(cancellationToken);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
using Microsoft.EntityFrameworkCore;
using MoscowWeather.Core.Models;
using MoscowWeather.Core.Repository;

namespace MoscowWeather.Infrastructure.Storage.Repositories;

public class WeatherArchiveRepository : IWeatherArchiveRepository
{
    private readonly MoscowWeatherDbContext _dbContext;

    public WeatherArchiveRepository(MoscowWeatherDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<WeatherArchive?> GetByIdAsync(long id, CancellationToken cancellationToken)
    {
        var archive = await _dbContext.Archives.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        return archive;
    }

    public async Task<List<WeatherArchive>> ListAsync(CancellationToken cancellationToken)
    {
        var archive = await _dbContext.Archives.OrderBy(x => x.Id).ToListAsync(cancellationToken);
        return archive;
    }

    public async Task AddAsync(WeatherArchive archive, CancellationToken cancellationToken)
    {
        await _dbContext.Archives.AddAsync(archive, cancellationToken);
    }

    public async Task RemoveAsync(WeatherArchive archive, CancellationToken cancellationToken)
    {
        _dbContext.Archives.Remove(archive);
    }

    public async Task SaveChangesAsync(CancellationToken cancellationToken)
    {
        await _dbContext.SaveChangesAsync(cancellationToken);
    }
}
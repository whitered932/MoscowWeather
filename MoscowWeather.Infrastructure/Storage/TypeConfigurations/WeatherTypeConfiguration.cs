using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MoscowWeather.Core.Models;

namespace MoscowWeather.Infrastructure.Storage.TypeConfigurations;

public class WeatherTypeConfiguration : IEntityTypeConfiguration<Weather>
{
    public void Configure(EntityTypeBuilder<Weather> builder)
    {
        builder.HasIndex(x => x.DateTimeUtc);
        builder.HasIndex(x => x.ArchiveId);
    }
}
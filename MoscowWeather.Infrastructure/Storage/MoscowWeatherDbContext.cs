using Microsoft.EntityFrameworkCore;
using MoscowWeather.Core.Models;

namespace MoscowWeather.Infrastructure.Storage;

public class MoscowWeatherDbContext : DbContext
{
    public MoscowWeatherDbContext(DbContextOptions options) : base(options)
    {
    }

    public MoscowWeatherDbContext()
    {
    }
    
    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfigurationsFromAssembly(typeof(MoscowWeatherDbContext).Assembly);
        base.OnModelCreating(modelBuilder);
    }
    public DbSet<WeatherArchive> Archives { get; set; }
    public DbSet<Weather> Weathers { get; set; }
}
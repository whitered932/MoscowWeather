﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MoscowWeather.Core;
using MoscowWeather.Core.Repository;
using MoscowWeather.Infrastructure.Services;
using MoscowWeather.Infrastructure.Storage;
using MoscowWeather.Infrastructure.Storage.Repositories;

namespace MoscowWeather.Infrastructure;

public static class DependencyInjection
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, string connectionString)
    {
        services.AddCore();
        services.AddDbContext<MoscowWeatherDbContext>(opt => opt.UseNpgsql(connectionString));
        services.AddScoped<IParseArchiveService, ParseArchiveService>();
        services.AddScoped<IWeatherArchiveRepository, WeatherArchiveRepository>();
        services.AddScoped<IWeatherRepository, WeatherRepository>();
        return services;
    }
}
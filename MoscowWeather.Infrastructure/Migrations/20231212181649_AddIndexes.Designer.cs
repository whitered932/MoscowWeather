﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using MoscowWeather.Infrastructure.Storage;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace MoscowWeather.Infrastructure.Migrations
{
    [DbContext(typeof(MoscowWeatherDbContext))]
    [Migration("20231212181649_AddIndexes")]
    partial class AddIndexes
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.13")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            NpgsqlModelBuilderExtensions.UseIdentityByDefaultColumns(modelBuilder);

            modelBuilder.Entity("MoscowWeather.Core.Models.Weather", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<long>("ArchiveId")
                        .HasColumnType("bigint");

                    b.Property<double?>("Cloudy")
                        .HasColumnType("double precision");

                    b.Property<DateTime>("DateTimeUtc")
                        .HasColumnType("timestamp with time zone");

                    b.Property<double?>("DewPoint")
                        .HasColumnType("double precision");

                    b.Property<double?>("HorizontalVisibility")
                        .HasColumnType("double precision");

                    b.Property<double?>("LowerCloudLimit")
                        .HasColumnType("double precision");

                    b.Property<double?>("Pressure")
                        .HasColumnType("double precision");

                    b.Property<double?>("RelativeHumidity")
                        .HasColumnType("double precision");

                    b.Property<double?>("Temperature")
                        .HasColumnType("double precision");

                    b.Property<string>("WeatherConditions")
                        .HasColumnType("text");

                    b.Property<string>("WindDirection")
                        .HasColumnType("text");

                    b.Property<double?>("WindSpeed")
                        .HasColumnType("double precision");

                    b.HasKey("Id");

                    b.HasIndex("ArchiveId");

                    b.HasIndex("DateTimeUtc");

                    b.ToTable("Weathers");
                });

            modelBuilder.Entity("MoscowWeather.Core.Models.WeatherArchive", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    NpgsqlPropertyBuilderExtensions.UseIdentityByDefaultColumn(b.Property<long>("Id"));

                    b.Property<byte[]>("Content")
                        .IsRequired()
                        .HasColumnType("bytea");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnType("text");

                    b.Property<int>("Status")
                        .HasColumnType("integer");

                    b.Property<DateTime>("UploadedAt")
                        .HasColumnType("timestamp with time zone");

                    b.HasKey("Id");

                    b.ToTable("Archives");
                });
#pragma warning restore 612, 618
        }
    }
}

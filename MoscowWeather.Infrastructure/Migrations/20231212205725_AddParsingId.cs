﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MoscowWeather.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddParsingId : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ParsingJobId",
                table: "Archives",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ParsingJobId",
                table: "Archives");
        }
    }
}

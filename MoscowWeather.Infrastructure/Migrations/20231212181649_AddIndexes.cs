﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MoscowWeather.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class AddIndexes : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Weathers_ArchiveId",
                table: "Weathers",
                column: "ArchiveId");

            migrationBuilder.CreateIndex(
                name: "IX_Weathers_DateTimeUtc",
                table: "Weathers",
                column: "DateTimeUtc");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Weathers_ArchiveId",
                table: "Weathers");

            migrationBuilder.DropIndex(
                name: "IX_Weathers_DateTimeUtc",
                table: "Weathers");
        }
    }
}

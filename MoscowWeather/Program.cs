using Hangfire;
using Hangfire.PostgreSql;
using MoscowWeather.Infrastructure;
using MoscowWeather.Infrastructure.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddMediatR((c) =>
{
    c.RegisterServicesFromAssembly(typeof(Program).Assembly);
});
builder.Services.AddInfrastructure(connectionString);
builder.Services.AddControllersWithViews();

builder.Services.AddHangfire(
    x => x.UsePostgreSqlStorage(
        c => c.UseNpgsqlConnection(connectionString)));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Archive}/{action=Index}/{id?}");

app.UseHangfireServer();
app.UseHangfireDashboard();

app.Run();
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MoscowWeather.Features.Archive;
using MoscowWeather.ViewModels;

namespace MoscowWeather.Controllers;

public class UploadController : BaseController
{
    private readonly IMediator _mediator;

    public UploadController(IMediator mediator)
    {
        _mediator = mediator;
    }

    public IActionResult Index()
    {
        return View();
    }
    
    [HttpPost]
    public async Task<ViewResult> Index(UploadDataViewModel model)
    {
        try
        {
            var command = new UploadArchivesCommand() {Archives = model.Files};
            await _mediator.Send(command);   
            ViewBag.Message = "File Uploaded Successfully!!";
            return View();
        }
        catch
        {
            ViewBag.Message = "File upload failed!!";
            return View();
        }
    }
}
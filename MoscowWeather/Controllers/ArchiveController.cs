﻿using System.Diagnostics;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using MoscowWeather.Features.Archive;
using MoscowWeather.Features.Weather;
using MoscowWeather.ViewModels;

namespace MoscowWeather.Controllers;

public class ArchiveController : BaseController
{
    private readonly IMediator _mediator;

    public ArchiveController(IMediator mediator)
    {
        _mediator = mediator;
    }

    public async Task<IActionResult> Index()
    {
        var archives = await _mediator.Send(new GetArchivesQuery());
        var model = new IndexArchiveViewModel() { Archives = archives };
        Response.Headers.Add("Refresh", "5");
        return View(model);
    }

    public async Task<IActionResult> Delete(long id)
    {
        await _mediator.Send(new DeleteArchiveCommand() { ArchiveId = id});
        return Redirect("/");
    }
    
    public async Task<IActionResult> Details(long id, List<int> months, List<int> years, int count = 15, int page = 1)
    {
        var model = await _mediator.Send(new GetWeathersQuery() { ArchiveId = id, Count = count, Page = page, Months = months, Years = years});
        return View(model);
    }

    public async Task<IActionResult> Stop(long id)
    {
        await _mediator.Send(new StopParsingArchiveCommand() { ArchiveId = id });
        return Redirect("/");
    }
}
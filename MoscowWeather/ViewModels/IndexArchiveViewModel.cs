using MoscowWeather.Core.Models;

namespace MoscowWeather.ViewModels;

public class IndexArchiveViewModel
{
    public IReadOnlyList<WeatherArchive> Archives { get; set; }  
}
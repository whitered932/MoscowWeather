using Microsoft.AspNetCore.Mvc.Rendering;
using MoscowWeather.Core.Models;

namespace MoscowWeather.ViewModels;

public class ArchiveDetailsViewModel
{
    public IReadOnlyList<Weather> Weathers { get; set; }
    public long Id { get; set; }
    public int PageCount { get; set; }
    public int CurrentPage { get; set; }
    public int StartPage { get; set; }
    public int EndPage { get; set; }
    public List<int> Months { get; set; }
    public List<int> SelectedYears { get; set; }
    public List<int> AvailableYears { get; set; }
}
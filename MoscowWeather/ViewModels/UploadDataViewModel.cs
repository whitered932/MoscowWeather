namespace MoscowWeather.ViewModels;

public class UploadDataViewModel
{
    public List<IFormFile> Files { get; set; }
}
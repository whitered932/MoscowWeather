using Hangfire;
using MediatR;
using MoscowWeather.Core.Models;
using MoscowWeather.Core.Repository;
using MoscowWeather.Core.Services;
using MoscowWeather.Infrastructure.Services;

namespace MoscowWeather.Features.Archive;

public class UploadArchivesCommand : IRequest<bool>
{
    public List<IFormFile> Archives { get; set; }
}

public sealed class UploadArchivesCommandHandler : IRequestHandler<UploadArchivesCommand, bool>
{
    private readonly IDateService _dateService;
    private readonly IWeatherArchiveRepository _weatherArchiveRepository;
    private readonly IParseArchiveService _parseArchiveService;
    private readonly IBackgroundJobClient _backgroundJob;

    public UploadArchivesCommandHandler(
        IDateService dateService,
        IWeatherArchiveRepository weatherArchiveRepository,
        IBackgroundJobClient backgroundJob,
        IParseArchiveService parseArchiveService)
    {
        _dateService = dateService;
        _weatherArchiveRepository = weatherArchiveRepository;
        _backgroundJob = backgroundJob;
        _parseArchiveService = parseArchiveService;
    }

    public async Task<bool> Handle(UploadArchivesCommand request, CancellationToken cancellationToken)
    {
        foreach (var formFile in request.Archives)
        {
            using var memoryStream = new MemoryStream();
            await formFile.CopyToAsync(memoryStream, cancellationToken);
            var archive = new WeatherArchive(memoryStream.ToArray(), _dateService.NowUtc(), formFile.FileName);
            await _weatherArchiveRepository.AddAsync(archive, cancellationToken);
            await _weatherArchiveRepository.SaveChangesAsync(cancellationToken);

            var jobId = _backgroundJob.Enqueue(() => _parseArchiveService.ParseAsync(archive.Id, CancellationToken.None));
            archive.SetParsingJobId(jobId);
            await _weatherArchiveRepository.SaveChangesAsync(cancellationToken);
        }

        return true;
    }
}
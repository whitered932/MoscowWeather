using Hangfire;
using MediatR;
using MoscowWeather.Core.Enums;
using MoscowWeather.Core.Repository;

namespace MoscowWeather.Features.Archive;

public class DeleteArchiveCommand : IRequest<bool>
{
    public long ArchiveId { get; set; }
}

public sealed class DeleteArchiveCommandHandler : IRequestHandler<DeleteArchiveCommand, bool>
{
    private readonly IWeatherArchiveRepository _archiveRepository;
    private readonly IWeatherRepository _weatherRepository;

    public DeleteArchiveCommandHandler(IWeatherArchiveRepository archiveRepository, IBackgroundJobClient backgroundJob, IWeatherRepository weatherRepository)
    {
        _archiveRepository = archiveRepository;
        _weatherRepository = weatherRepository;
    }

    public async Task<bool> Handle(DeleteArchiveCommand request, CancellationToken cancellationToken)
    {
        var archive = await _archiveRepository.GetByIdAsync(request.ArchiveId, cancellationToken);
        if (archive is null)
        {
            return false;
        }
        await _archiveRepository.RemoveAsync(archive, cancellationToken);

        var weathers = await _weatherRepository.ListAsync(archive.Id, cancellationToken);
        await _weatherRepository.RemoveRangeAsync(weathers);
        
        await _archiveRepository.SaveChangesAsync(cancellationToken);
        return true;
    }
}
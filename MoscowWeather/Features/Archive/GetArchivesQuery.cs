using MediatR;
using MoscowWeather.Core.Models;
using MoscowWeather.Core.Repository;

namespace MoscowWeather.Features.Archive;

public class GetArchivesQuery : IRequest<IReadOnlyList<WeatherArchive>> 
{
    
}

public sealed class GetArchivesQueryHandler : IRequestHandler<GetArchivesQuery, IReadOnlyList<WeatherArchive>>
{
    private readonly IWeatherArchiveRepository _archiveRepository;

    public GetArchivesQueryHandler(IWeatherArchiveRepository archiveRepository)
    {
        _archiveRepository = archiveRepository;
    }

    public async Task<IReadOnlyList<WeatherArchive>> Handle(GetArchivesQuery request, CancellationToken cancellationToken)
    {
        return await _archiveRepository.ListAsync(cancellationToken);
    }
}
using Hangfire;
using MediatR;
using MoscowWeather.Core.Enums;
using MoscowWeather.Core.Repository;

namespace MoscowWeather.Features.Archive;

public class StopParsingArchiveCommand : IRequest<bool>
{
    public long ArchiveId { get; set; }
}

public sealed class StopParsingArchiveCommandHandler : IRequestHandler<StopParsingArchiveCommand, bool>
{
    private readonly IWeatherArchiveRepository _archiveRepository;
    private readonly IBackgroundJobClient _backgroundJob;

    public StopParsingArchiveCommandHandler(IWeatherArchiveRepository archiveRepository, IBackgroundJobClient backgroundJob)
    {
        _archiveRepository = archiveRepository;
        _backgroundJob = backgroundJob;
    }

    public async Task<bool> Handle(StopParsingArchiveCommand request, CancellationToken cancellationToken)
    {
        var archive = await _archiveRepository.GetByIdAsync(request.ArchiveId, cancellationToken);
        if (archive is null)
        {
            return false;
        }
        _backgroundJob.Delete(archive.ParsingJobId);
        archive.UpdateStatus(ArchiveStatus.Cancelled);
        await _archiveRepository.SaveChangesAsync(cancellationToken);
        return true;
    }
}
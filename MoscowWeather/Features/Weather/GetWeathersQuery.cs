using MediatR;
using MoscowWeather.Core.Repository;
using MoscowWeather.ViewModels;

namespace MoscowWeather.Features.Weather;

public class GetWeathersQuery : IRequest<ArchiveDetailsViewModel>
{
    public long ArchiveId { get; set; }
    public int Page { get; set; }
    public int Count { get; set; }

    public List<int> Months { get; set; } = new();
    public List<int> Years { get; set; } = new();
}

public sealed class GetWeatherQueryHandler : IRequestHandler<GetWeathersQuery, ArchiveDetailsViewModel>
{
    private readonly IWeatherRepository _weatherRepository;

    public GetWeatherQueryHandler(IWeatherRepository weatherRepository)
    {
        _weatherRepository = weatherRepository;
    }

    public async Task<ArchiveDetailsViewModel> Handle(GetWeathersQuery request, CancellationToken cancellationToken)
    {
        var weathers =
            await _weatherRepository.ListAsync(
                request.Page,
                request.Count,
                request.ArchiveId,
                request.Months,
                request.Years,
                cancellationToken);
        var availableYears = await _weatherRepository.GetYears(request.ArchiveId, cancellationToken);
        float weathersCount = await _weatherRepository.Count(request.ArchiveId, request.Months, request.Years, cancellationToken);
        var totalPages = (int)Math.Ceiling(weathersCount / request.Count);
        var (startPage, endPage) = GetPages(request.Page, totalPages);

        var model = new ArchiveDetailsViewModel()
        {
            Id = request.ArchiveId,
            Weathers = weathers,
            CurrentPage = request.Page,
            PageCount = totalPages,
            StartPage = startPage,
            EndPage = endPage,
            SelectedYears = request.Years,
            AvailableYears = availableYears,
            Months = request.Months
        };

        return model;
    }

    private static (int startPage, int endPage) GetPages(int currentPage, int totalPages)
    {
        int startPage;
        int endPage;
        switch (currentPage)
        {
            case <= 9:
                startPage = 1;
                endPage = 16;
                break;
            case > 9:
                startPage = currentPage - 7;
                endPage = currentPage + 7;
                break;
        }

        if (totalPages - currentPage < 12)
        {
            startPage = totalPages - 15;
            endPage = totalPages;
        }

        return (startPage, endPage);
    }
}